## System requirements:

1. `python3`
2. `pip`
3. `sqlite3`

## To setup the project clone it and run:

1. `python3 -m venv venv`
2. `source venv/bin/activate`
3. `make`
4. `make run`
