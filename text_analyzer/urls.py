from home import views as home_views
from django.urls import path

urlpatterns = [
    path('', home_views.HomeView.as_view(), name='home'),
    path('upload', home_views.upload_file, name='upload'),
    path('archive', home_views.ArchiveView.as_view(), name='archive'),
    path('<int:pk>/results', home_views.ResultsView.as_view(), name='results'),
    path('<int:record_id>/plot', home_views.generate_histogram, name='plot')
]
