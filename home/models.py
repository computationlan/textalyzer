from codecs import iterdecode
import csv
from io import BytesIO
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from textblob import TextBlob
from django.db import models
from django.core.exceptions import ValidationError

def validate_file_contents(file):
    '''
    Verify input file format: 
    - must be English language
    - must be at least 10 words
    '''
    file.seek(0)
    text = TextBlob(file.read().decode('utf-8'))
    if len(text.words) < 10:
        raise ValidationError('Document is too short')
    lang = text.detect_language() 
    if lang != 'en':
        raise ValidationError(f'Unsupported language: {lang}')

class Record(models.Model):
    # Provided by the user.
    description = models.CharField(max_length=255)
    # Original file is stored for future re-processing.
    datafile = models.FileField(upload_to='uploads/', validators=[validate_file_contents])
    uploaded_at = models.DateTimeField(auto_now_add=True)
    processed_at = models.DateTimeField()
    # Provided by the user.
    user_name = models.CharField(max_length=255)
    # Fields below are derived from datafile and are stored in the DB for performance reasons.
    # Range [-1, 1].
    polarity = models.FloatField()
    # Range [0, 1].
    subjectivity = models.FloatField()

    def __str__(self):
        return self.description

    def polarity_text(self):
        if self.polarity < -0.5:
            return "very negative"
        if self.polarity < 0:
            return "somewhat negative"
        if self.polarity < 0.5:
            return "somewhat positive"
        return "very positive"

    def subjectivity_text(self):
        if self.subjectivity < 0.3:
            return "very objective"
        if self.subjectivity < 0.7:
            return "neither subjective nor objective"
        return "very subjective"

    def histogram(self):
        '''
        Visualize text's polarity compared to all other samples in the system. 
        Returns a PNG image.
        '''  
        # Using hardcoded made-up numbers for testing. For production this should be based on actual data.
        weight = [9, 19, 38, 69, 119, 192, 288, 406, 536, 663, 768, 833, 847, 806, 719, 601, 471, 345, 237, 153, 92, 52, 27, 13]
        num_buckets = len(weight)
        buckets = np.linspace(-1 + 1. / num_buckets, 1 - 1. / num_buckets, num_buckets)

        def selected(bucket):
            return bucket - 1. / num_buckets <= self.polarity < bucket + 1. / num_buckets
        colors = ['red' if selected(bucket) else 'dodgerblue' for bucket in buckets]

        matplotlib.use('Agg')
        plt.bar(buckets, weight, 2. / num_buckets, color=colors)
        # Axes and labels.
        plt.xticks(np.linspace(-1, 1, 10))
        plt.yticks([])
        plt.tight_layout()

        # Write image to output string.
        buffer = BytesIO()
        plt.savefig(buffer, format='png')
        return buffer.getvalue()
