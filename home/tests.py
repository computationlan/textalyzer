import datetime
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls import reverse
from .models import Record

class IndexTestCase(TestCase):
    def test_app_has_index(self):
        resp = self.client.get('/')
        self.assertContains(resp, 'Welcome')

def create_record(description):
    '''
    Create new database record of text data.
    '''
    return Record.objects.create(
        description=description, 
        datafile='datafile', 
        processed_at=datetime.datetime.now(), 
        uploaded_at=datetime.datetime.now(), 
        user_name='L', 
        polarity=-0.2,
        subjectivity=0.7,
    )

class RecordTestCase(TestCase):
    def test_model_functions(self):
        '''
        Checking internal methods of a database record object.
        '''
        record = create_record('my description')
        self.assertEqual(record.polarity_text(), 'somewhat negative')
        self.assertEqual(record.subjectivity_text(), 'very subjective')
        self.assertIn(b'PNG', record.histogram())

class ArchiveViewTests(TestCase):
    def test_empty_db_resp(self):
        """
        The archive view with empty DB returns a correct message.
        """
        resp = self.client.get(reverse('archive'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'No files are uploaded yet.')
        self.assertQuerysetEqual(resp.context['record_list'], [])

    def test_two_records(self):
        """
        The archive view with two records in the DB returns appropriate message and displays appropriate data.
        """
        create_record('description 1')
        create_record('description 2')
        resp = self.client.get(reverse('archive'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'description 1')
        self.assertContains(resp, 'description 2')
        self.assertQuerysetEqual(resp.context['record_list'], 
            ['<Record: description 1>', '<Record: description 2>'],
            ordered=False)

class ResultsViewTests(TestCase):
    def test_empty_db_resp(self):
        """
        The results view with non-existing record number returns a correct message.
        """
        resp = self.client.get(reverse('results', args=(42,)))
        self.assertEqual(resp.status_code, 404)

    def test_record_exists(self):
        """
        The results view for a record returns appropriate message and displays appropriate data.
        """
        create_record('my description')
        resp = self.client.get(reverse('results', args=(1,)))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'Your document is somewhat negative and very subjective.')
        self.assertContains(resp, 'Document description: my description')
        self.assertEqual(resp.context['record'].description, 'my description')

class HistogramViewTests(TestCase):
    def test_empty_db_resp(self):
        """
        The histogram view with non-existing record number returns a correct message.
        """
        resp = self.client.get(reverse('plot', args=(42,)))
        self.assertEqual(resp.status_code, 404)

    def test_record_exists(self):
        """
        The histogram view for an existing record number returns a correct message and displays appropriate data.
        """
        create_record('my description')
        resp = self.client.get(reverse('plot', args=(1,)))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, b'PNG')

class UploadViewTests(TestCase):
    def upload_file(self, contents):
        """
        Peforming file upload via POST request and returning the response.
        """
        return self.client.post(reverse('upload'), {
            "datafile": SimpleUploadedFile('test_file.txt', contents), 
            "description": 'test description', 
            "user_name": 'test user name',
        })

    def test_without_upload(self):
        """
        Checking that the GET request to file upload page returns an appropriate message&status.
        """
        resp = self.client.get(reverse('upload'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, '<form')
        self.assertContains(resp, 'Upload')

    def test_correct_upload(self):
        """
        Checking that a valid file upload results returns an appropriate response&status and populates the database.
        """
        resp = self.upload_file(b'This test is awesome. I am very happy about it.')
        self.assertEqual(resp.status_code, 302)
        record = Record.objects.get(pk=1)
        self.assertEqual(record.description, 'test description')
        self.assertEqual(record.user_name, 'test user name')
        self.assertGreater(record.polarity, 0)
        self.assertGreater(record.subjectivity, 0.5)

    def test_too_short_upload(self):
        """
        Checking an invalid file upload returns an appropriate status and error message.
        """
        resp = self.upload_file(b'not enough words')
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'errorlist')
        self.assertContains(resp, 'too short')

    def test_wrong_language_upload(self):
        """
        Checking an invalid file upload returns an appropriate status and error message.
        """
        resp = self.upload_file('Ce test est génial. Je suis très content de ça.'.encode('utf-8'))
        self.assertEqual(resp.status_code, 200)
        self.assertContains(resp, 'errorlist')
        self.assertContains(resp, 'language')
