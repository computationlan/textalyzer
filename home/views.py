from codecs import iterdecode
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views.generic import View, ListView, DetailView
from textblob import TextBlob
from .forms import RecordForm
from .models import Record

class HomeView(View):
    def get(self, request):
        return render(request, 'home.html')

class ArchiveView(ListView):
    model = Record
    template_name = 'archive.html'

class ResultsView(DetailView):
    model = Record
    template_name = 'results.html'

def upload_file(request):
    if request.method == 'POST':
        form = RecordForm(request.POST, request.FILES)
        if form.is_valid():
            record = form.save(commit=False)
            # Precompute a bunch of data and save it to the database, since it could be slow 
            # in the real system and we don't want to redo it each time we display results to the user.
            record.datafile.seek(0)
            text = TextBlob(record.datafile.read().decode('utf-8'))
            record.polarity = text.polarity
            record.subjectivity = text.subjectivity
            record.processed_at = timezone.now()
            record.save()
            return HttpResponseRedirect(reverse('results', args=(record.id,)))
    else:
        form = RecordForm()
    context = {'form': form}
    return render(request, 'upload_file.html', context)

def generate_histogram(request, record_id):
    record = get_object_or_404(Record, pk=record_id)
    return HttpResponse(record.histogram(), content_type='image/png')
