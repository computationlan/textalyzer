all:
	pip install -r requirements.txt
	python -m textblob.download_corpora lite
	./manage.py makemigrations
	./manage.py migrate

test:
	./manage.py test

run:
	./manage.py runserver

.PHONY: test run
